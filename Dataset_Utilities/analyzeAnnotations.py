import os
import pandas as pd
import argparse



def analyzeAnnotations(args):
    '''
    Takes an AAU Bounding Box annotation csv file and analyses the occurence of each category, and save to a provide output file

    Input:
        args: Dict containing the following elements
            - inputFile: Path to the input file
            - outputFile Path to the output file
    '''

    filename = args["inputFile"]

    annotations = pd.read_csv(filename, sep = ";")

    unqiue_tags = annotations["Annotation tag"].unique()


    with open(args["outputFile"], "w") as f:        
        # For each unique category in the annotaitons, go through and count the category occurence and amount of unique videos it occurs in
        for tag in sorted(unqiue_tags):
            df_tag = annotations[annotations["Annotation tag"] == tag]
            
            occurences = len(df_tag)
            video_occurences = df_tag["Filename"].map(lambda x: x[:-9])
            video_occurences = len(video_occurences.unique())

            f.write("Tag: {}\tTotal Occurences: {}\tUnique video occurences: {}\n".format(tag, occurences, video_occurences))
        f.write("All  \tTotal Occurences: {}\tUnique video occurences: {}".format(len(annotations), len((annotations["Filename"].map(lambda x: x[:-9]).unique()))))


if __name__ == "__main__":
    ap = argparse.ArgumentParser(
            description = "Takes a an annotation csv file and analyze it")
    ap.add_argument("-inputFile", "--inputFile", type=str, default = "collected.csv",
                    help="Path to the annotation csv file")
    ap.add_argument("-outputFile", "--outputFile", type=str, default = "annotationOverview.txt",
                    help="Path to the output file")
    
    args = vars(ap.parse_args())
    
    analyzeAnnotations(args)