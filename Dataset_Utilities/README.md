## Dataset preprocessing

These scripts are for preprocessing the dataset, and creating the necessary files for analysing the data.

*extractFrames.py* takes a directory with videos, and extract the video frames.

*createImageList.py* takes an image directory and creates a .txt file with each frame's absolute path on separate lines

*createDatasetSplit.py* takes an image list and splits the dataset into train, validation and testing splits.

*collectAnnotations.py* searches through a directory and finds all annotation files in the [AAU Bounding Box format](https://bitbucket.org/aauvap/bounding-box-annotator/wiki/Home), and collects all annotations in a singel csv file.

*analyzeAnnotations.py* analyses the provided csv file and calculates the occurence rate of each category.

*imagelistToAnnotations.py* takes an imagelist and annotation csv, and creates a new annotation csv with just the frames specified in the image list.

*createDummyYOLOAnnotations.py* creates empty .txt files for the frames in a dir with no detections in it.