import os
import pandas as pd
import argparse
import numpy as np


def imagelistToAnnotations(args):
    '''
    Takes an imagelist and an AAU Bounding Box annotation csv, and extracts the annotations in the csv file that corresponds with images in the imagelist
    
    Input:
        args: Dict containing the following elements
            - inputAnnotations: Path to the annotation csv file
            - inputImageList: Path to the image list txt file
            - outputFile: Path to the output (subset) annotation csv file
    '''

    annotation_df = pd.read_csv(args["inputAnnotations"],sep=";")
    imagelist_file = open(args["inputImageList"], "r")
    filenames = imagelist_file.read().split('\n')
    imagelist_file.close()

    df = pd.DataFrame()
    for filename in filenames:
        filename = os.path.basename(filename)
        df_file = annotation_df[annotation_df["Filename"] == filename]
        df = df.append(df_file)

    df.to_csv(args["outputFile"], sep=";")



if __name__ == "__main__":
    ap = argparse.ArgumentParser(
            description = "Takes a an annotation csv file and analyze it")
    ap.add_argument("-inputAnnotations", "--inputAnnotations", type=str, default = "collected.csv",
                    help="Path to the annotation csv file")
    ap.add_argument("-inputImageList", "--inputImageList", type=str, default = "train.txt",
                    help="Path to the image list file")
    ap.add_argument("-outputFile", "--outputFile", type=str, default = "train.csv",
                    help="Path to the output annotation csv file")
    
    args = vars(ap.parse_args())
    
    imagelistToAnnotations(args)