import os
import argparse
import numpy as np
import pandas as pd

def collectAnnotations(args):
    '''
    Goes through all dirs provided as input and finds all files called 'annotations.csv'. 
    All annotations are stored and saved in a single combined csv file, follwoing the AAU Bounding Box annotation format

    Input:
        args: Dict containing the following elements
            - inputFolder: Path to the input folder
            - outputFile Path to the output file
            - reindexIDs: Boolean indicating whether to reindex the object IDs, so that each ID is unique in the collected video
    '''

    rootdir = args["inputFolder"]
    reindexIDs = args["reindexIDs"]

    collected = None
    idCount = 1

    for root, _, files in os.walk(rootdir):
        for filename in files:
            if filename.lower() == "annotations.csv":
                annotations = pd.read_csv(os.path.join(root, filename), sep=";", usecols=np.arange(8))
                ids = annotations["Object ID"].unique()

                if reindexIDs:
                    idMapDir = {}
                    for id in ids:
                        idMapDir[id] = idCount
                        idCount += 1
                    annotations = annotations.replace({'Object ID': idMapDir})
                
                if collected is None:
                    collected = annotations
                else:
                    collected = collected.append(annotations)

    collected.to_csv(args["outputFile"], sep=";", index=False)



if __name__ == "__main__":
    ap = argparse.ArgumentParser(
            description = "Takes a root dir and creates a collected annoation file")
    ap.add_argument("-inputFolder", "--inputFolder", type=str, default = "D:\Limfjordsbro-CVPRW\Annotated_data",
                    help="Path to the main folder holding all images and corresponding annotation files")
    ap.add_argument("-outputFile", "--outputFile", type=str, default = "collected.csv",
                    help="Path to the output folder")
    ap.add_argument("-reindexIDs", "--reindexIDs", action='store_true',
                    help="Reindex the Object IDs")
    
    args = vars(ap.parse_args())
    
    collectAnnotations(args)