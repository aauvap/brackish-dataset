## YOLO models

Contains the config files for each YOLO model trained and tested.

AAU = Brackish dataset labels

viame = VIAME labels

openimages = Model with full openimages output classes

OI = Model with only relevant openimage output classes