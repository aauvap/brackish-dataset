## Detection of Marine Animals in a New Underwater Dataset with Varying Visibility

This repository contains the code and scripts for the paper *Detection of Marine Animals in a New Underwater Dataset with Varying Visibility*

The paper investigates detection of underwater creatures, in environments with varying visibility. Two object detectors were fine tuned and compared, YOLOv2 and YOLOv3, and evalauted using the COCO metrics.

As this is a collection of research code, one might find some occational rough edges. We have tried to clean up the code to a decent level but if you encounter a bug or a regular mistake, please report it in our issue tracker. 

### The AAU Brackish Dataset 

The evaluation code is built around The Brackish dataset which is published on [Kaggle](https://www.kaggle.com/aalborguniversity/brackish-dataset), recorded in Limfjorden at Aalborg, Denmark.

### Code references

The networks were fine-tuned using the [Darknet](https://pjreddie.com/darknet/) framework, utilizing the OpenImages pre-traiend weights.

The Detections were performed using the [VIAME](https://github.com/VIAME/VIAME) framework, Version 0.9.9.7 GPU binaries for Windows, and evalauted using the [MS COCO](http://cocodataset.org/) evaluation code.

### License

All code is licensed under the MIT license, except for the modified VIAME and COCO evaluation code, which are subject to their respective licenses

### Acknowledgements
Please cite the following paper if you use our code or dataset:

```TeX
@InProceedings{Pedersen_2019_CVPR_Workshops,
author = {Pedersen, Malte and Haurum, Joakim Bruslund and Gade, Rikke and Moeslund, Thomas B. and Madsen, Niels},
title = {Detection of Marine Animals in a New Underwater Dataset with Varying Visibility},
booktitle = {The IEEE Conference on Computer Vision and Pattern Recognition (CVPR) Workshops},
month = {June},
year = {2019}
} 
```