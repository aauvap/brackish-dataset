from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
import os
import argparse

# Global Dicts containg the cateogry IDs for the relvant categories when using OpenImages, VIAME and The Brackish Dataset categories.
OICat = {76: "Starfish",
         463: "Fish",
         488: "Jellyfish",
         582: "Shrimp",
         589: "Crab"
         }
VIAMECat = {1: "vertebrate",
            2: "invertebrate"
         }
AAUCat = {1: "Fish",
          2: "Small_fish",
          3: "Crab",
          4: "Shrimp",
          5: "Jellyfish",
          6: "Starfish"
         }
         
def writeCOCOtoTXT(stats, output_file, cat):
        '''
        Writes the provided MS COCO metric results for a given cateogry to a txt file (appends if the file already exists)

        Input:
            - stats: List containing the results for the 12 MS COCO metric
            - output-file: Path to the output file
            - cat: Cateogry which has been analyzed
        '''

        with open(output_file, "a") as f:
                f.write("Category:\t{}\n".format(cat))
                f.write("Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = {}\n".format(stats[0]))
                f.write("Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = {}\n".format(stats[1]))
                f.write("Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = {}\n".format(stats[2]))
                f.write("Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = {}\n".format(stats[3]))
                f.write("Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = {}\n".format(stats[4]))
                f.write("Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = {}\n".format(stats[5]))
                f.write("Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=1   ] = {}\n".format(stats[6]))
                f.write("Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=10  ] = {}\n".format(stats[7]))
                f.write("Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = {}\n".format(stats[8]))
                f.write("Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = {}\n".format(stats[9]))
                f.write("Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = {}\n".format(stats[10]))
                f.write("Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = {}".format(stats[11]))
                f.write("\n\n")




def COCOEvaluation(args):
    '''
    Compares detections and annotations using the MS COCO metrics.
    
    Input:
        args: Dict containing the following elements
            - annotationJSON: Path to the annotation file
            - detectionJSON: Path to detection file
            - annotationType: Annotation type used for determining how the metrics are computed
    '''

    annotations = args["annotationJSON"]
    detections = args["detectionsJSON"]
    annType = args["annotationType"]

    annotations_name = os.path.basename(annotations)[:-5]
    detections_name = os.path.basename(detections)[:-17]

    #initialize COCO ground truth api
    cocoGt=COCO(annotations)

    # Load the correct categroy IDs depending on the used annotations
    if "OI" in annotations_name:
            catDict = OICat
    elif "VIAME" in annotations_name:
            catDict = VIAMECat
    elif "AAU" in annotations_name:
            catDict = AAUCat
                    

    #initialize COCO detections apih
    cocoDt=cocoGt.loadRes(detections)
    imgIds=sorted(cocoGt.getImgIds())

    # Running evaluation
    cocoEval = COCOeval(cocoGt,cocoDt,annType)
    cocoEval.params.imgIds  = imgIds
    cocoEval.evaluate()
    cocoEval.accumulate()
    cocoEval.summarize()

    # Write metrics to txt file
    writeCOCOtoTXT(cocoEval.stats, detections_name +".txt", "all")

    # Wirte metric stats per category to tct files
    for catId, cat in catDict.items():
        cocoClassEval = COCOeval(cocoGt,cocoDt,annType)
        cocoClassEval.params.imgIds  = imgIds
        cocoClassEval.params.catIds = [catId]
        cocoClassEval.evaluate()
        cocoClassEval.accumulate()
        cocoClassEval.summarize()
        writeCOCOtoTXT( cocoClassEval.stats,detections_name+".txt", cat)



    
if __name__ == "__main__":
    ap = argparse.ArgumentParser(
            description = "Takes a set of annotations and detections and determines the performance using the MS COCO metrics")
    ap.add_argument("-annotationJSON", "--annotationJSON", type=str, default = "D:\Limfjordsbro-CVPRW\Scripts\Format_conversion\VIAME_TEST_groundtruth.json",
                    help="Path to the MS COCO anotation JSON file")
    ap.add_argument("-detectionsJSON", "--detectionsJSON", type=str, default = "D:\Limfjordsbro-CVPRW\Scripts\Format_conversion\computed_detections_yolov2_VIAME_coco_output.json",
                    help="Path to the MS COCO detection JSON file ")
    ap.add_argument("-annotationType", "--annotationType", type=str, default = "bbox",
                    help="type of the annotations. Default is 'bbox'")
    args = vars(ap.parse_args())
    
    COCOEvaluation(args)