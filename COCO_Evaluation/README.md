Clone of Clone of COCO API - http://cocodataset.org/ / https://github.com/philferriere/cocoapi
===========================================

# Original README:

COCO is a large image dataset designed for object detection, segmentation, person keypoints detection, stuff segmentation, and caption generation. This package provides Matlab, Python, and Lua APIs that assists in loading, parsing, and visualizing the annotations in COCO. Please visit http://cocodataset.org/ for more information on COCO, including for the data, paper, and tutorials. The exact format of the annotations is also described on the COCO website. The Matlab and Python APIs are complete, the Lua API provides only basic functionality.


# Additions

Added a script *COCOEvaluation.py* which calculates the COCO metrics for all classes and pre class


