## Annotation format conversion

These scripts are for transforming between [AAU Bounding Box annotations](https://bitbucket.org/aauvap/bounding-box-annotator/wiki/Home) to MS COCO format (*AnnotationsToCOCO.py*), and from the VIAME annotations format to MS COCO (*VIAMEToCOCO.py*).
