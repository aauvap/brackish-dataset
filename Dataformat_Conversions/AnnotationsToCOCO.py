import cv2
import json
import numpy as np
import os
import pandas as pd
import argparse

def importCategories(name_file):
    '''
    Takes filepath to file with category names. Each line contains a new category name.
    Output at tuple of two dicts, containing the mapping from cateogry name to cateogry index, and back

    Input:
        name_file: str
    
    Output:
        categoryLabels: dict, containing the mapping from categroy number to label
        categoryNumbers: dict, containing the mapping from categroy label to number
    '''
    # Import category names list
    categoryLabels = dict()
    categoryNumbers = dict()

    with open(name_file) as f:
        lineNumber = 1

        for line in f:
            entries = line.replace('\n', '')

            if len(entries) > 1:
                number = lineNumber
                label = entries

                categoryLabels[int(number)] = label
                categoryNumbers[label] = int(number)

            lineNumber += 1

    return (categoryLabels, categoryNumbers)


def imageToDictEntry(imagePath, imageId, shortPath):
    '''
    Takes an image and creates the base entry for MS COCO.

    Input:
        imagePath: Filepath to image
        imageId: Integer describing which image it is
        shortPath: The short name/alias for this image
    
    Output:
        image: Dict containing the basic information of the image [id, width, height, file_name, license]
    '''

    imageFile = cv2.imread(imagePath)

    image = dict()

    if imageFile is not None:
        height, width, _ = imageFile.shape

        # Image dictionary
        image = dict()
        image['id'] = imageId
        image['width'] = width
        image['height'] = height
        image['file_name'] = shortPath
        image['license'] = 0
    else:
        print("Could not read: " + imagePath)

    return image

def annotationToDictEntry(df_entry, annotationId, imageToIDDict, categoryToNumberDict):
    '''
    Takes a single annotation in AAU Bounding Box format and converts it to MS COCO format.

    Input:
        df_entry. A Pandas series, containing the annotation information
        annotationId: An integer indicating which annotation it is
        imageToIDDict: Dict converting the frame name to a the corresponding unique ID
        categoryToNumberDict: Dict converting category name to category number

    Output:
        annotation: Dict containing the annotation as per MS COCO bounding box format
    '''

    annotation = dict()

    annotation["iscrowd"] = 0
    annotation["id"] = annotationId
    annotation["image_id"]= imageToIDDict[df_entry["Filename"]]
    annotation["category_id"] = categoryToNumberDict[df_entry["Annotation tag"]]
    tl_x = df_entry["Upper left corner X"]
    tl_y = df_entry["Upper left corner Y"]
    br_x = df_entry["Lower right corner X"]
    br_y = df_entry["Lower right corner Y"]
    width = br_x-tl_x
    height = br_y-tl_y
    annotation["bbox"] = [tl_x, tl_y, width, height]
    annotation["area"] = width*height

    return annotation

def createCategoryList(categoryLabels):
    '''
    Creates a list of categories according to the MS COCO format. Currently does not support super categories

    Input 
        categoryLabels:Dict containing mapping from categroy number to category name

    Output:
        categories: List of dict. Each dict contains the supercategory, id and name per category

    '''

    categories = []

    for catId, name in categoryLabels.items():
        catEntry = dict()
        catEntry['supercategory'] = 'None'
        catEntry['id'] = catId
        catEntry['name'] = name

        categories.append(catEntry)

    return categories


def AAUToCOCO(args):
    '''
    Maps AAU Bounding Box annotations to MS COCO boudning box annotations
    
    Input:
        args: Dict containing the following elements
            - datasetName: Name of the dataset
            - imageList: Path to file containing the paths to the images
            - annotationCSV: Path to the AAU Bounding Box annotation csv file
            - categories: Path to file containing the category names

    Outputs two json files.
        '"datasetName"_groundtruth.json': Contains the annotations in MS COCO format
        '"datasetName"_helper_dirs.json': contains the mappings from cateogry names <-> cateogry number and image <-> ID look up tables
    '''

    # Read input dict
    datasetName = args["datasetName"]
    imagelistPath = args["imageList"]
    annotationPath = args["annotationCSV"]
    categoryfilePath = args["categories"]

    # Import categories
    categoryLabels, categoryNumbers = importCategories(categoryfilePath)
    categoryList = createCategoryList(categoryLabels)
    

    # Read image paths
    with open(imagelistPath, 'r') as f:
        imagelist = f.readlines()
    imagelist = [x.rstrip() for x in imagelist]

    # Load annotation csv
    annotations_df = pd.read_csv(annotationPath, sep=";")


    images = []
    annotations = []
    idToImageLookupTable = dict()
    imageToIdLookupTable = dict()
    imageIdCounter = 0
    annotationIdCounter = 0

    # Get basic information for each image for the MS COCO format
    for filename in imagelist:
        if os.path.splitext(filename)[-1] == ".png":
            basename = os.path.basename(filename)
            imageIdCounter += 1 
            image = imageToDictEntry(filename, 
                                    imageIdCounter, 
                                    basename)

            # Add image to the image <-> Id LUTs
            idToImageLookupTable[imageIdCounter] = basename
            imageToIdLookupTable[basename] = imageIdCounter

            images.append(image)


    # For each annotation, convert to MS COCO format
    for _, row in annotations_df.iterrows():
        annotationIdCounter += 1
        annotation = annotationToDictEntry(row, annotationIdCounter, imageToIdLookupTable, categoryNumbers)
        annotations.append(annotation)


    # Basic dataset infromation
    dataset_info = {'Description': datasetName,
                    "url": "",
                    "version": "0.0.1",
                    "year": 2019,
                    "Contributor": "Visual Analysis of People lab, AAU",
                    "date_created": "12-04-2019"}

    # Collect and saveall the basic dataset_info and the converted image and annotation information
    gtDict = dict(info = dataset_info, 
                licenses = ['MIT'],
                images = images, 
                annotations = annotations,
                categories = categoryList)

    with open(datasetName +'_groundtruth.json', 'w') as outfile:
        json.dump(gtDict, outfile)

    # Save the used LUTs
    lookupDict = dict(idToImageLookupTable = idToImageLookupTable,
                    imageToIdLookupTable = imageToIdLookupTable,
                    categoryNametoId = categoryNumbers,
                    categoeryIdtoName = categoryLabels)

    with open(datasetName+'_helper_dirs.json', 'w') as outfile:
        json.dump(lookupDict, outfile)


    
if __name__ == "__main__":
    ap = argparse.ArgumentParser(
            description = "Converts the ground truth annotations from AAU Bounding Box annotation format to MS COCO format")
    ap.add_argument("-imageList", "--imageList", type=str,
                    help="Path to imagelist text file")
    ap.add_argument("-annotationCSV", "--annotationCSV", type=str,
                    help="Path to csv file with AAU Bounding Box annotations")
    ap.add_argument("-categories", "--categories", type=str,
                    help="Path to file with categories")
    ap.add_argument("-datasetName", "--datasetName", type=str,
                    help="The dataset name")
    args = vars(ap.parse_args())
    
    AAUToCOCO(args)