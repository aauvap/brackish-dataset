## Detection postprocessing

These scripts are based on the [VIAME](https://github.com/VIAME/VIAME) framework and used to analyse the output of the YOLO detection output.

Weights are not included.