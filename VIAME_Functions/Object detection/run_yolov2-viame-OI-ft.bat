@echo off

REM Setup VIAME Paths (no need to set if installed to registry or already set up)

SET VIAME_INSTALL=C:\Program Files\VIAME

CALL "%VIAME_INSTALL%\setup_viame.bat"

REM Run Pipeline

pipeline_runner.exe -p "..\pipelines\detector_yolov2_viame-OI-ft.pipe" ^
                    -s input:video_filename=test.txt

pause
